package helper;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;


public class ConfigProperties {

    // private static final  Properties ConfigDataProperties=null;
    private static Properties ConfigDataProperties = null;
    private static FileInputStream file = null;
    private final static String ConfigData_CONFIG_PATH = System.getProperty("user.dir") + "/" + "src/main/java/config/ConfigData.properties";


    public static void ReadConfigDataProperties() throws IOException {

        file = new FileInputStream(ConfigData_CONFIG_PATH);
        ConfigDataProperties = new Properties();
        ConfigDataProperties.load(file);

    }

    public String getName() {
        return ConfigDataProperties.getProperty("Name");
    }
    public String getEmail() {
        return ConfigDataProperties.getProperty("Email");
    }

    public String getPassword() {
        return ConfigDataProperties.getProperty("password");
    }

}
