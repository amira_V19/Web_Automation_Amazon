package pages;

import helper.GUIActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import static helper.GUIActions.*;

public class LoginPage {
    private final WebDriver driver;
    private final GUIActions guiActions;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        guiActions = new GUIActions(driver);

    }

    private final By mobOrEmailTxt = By.id("ap_email");
    private final By continueButton = By.id("continue");
    private final By passwordField = By.id("ap_password");
    private final By submitButton = By.id("signInSubmit");
    private final By accountLists = By.xpath("//*[@id=\"nav-link-accountList\"]");


    public void setMobOrEmail(String mobOrEmail)
    {
        setText(mobOrEmailTxt, mobOrEmail);
        clickButton(continueButton);
    }

    public void setPassword(String password)
    {
        setText(passwordField, password);
        clickButton(submitButton);
    }

    public String getAlertText() {
        return getText(accountLists);
    }

}
