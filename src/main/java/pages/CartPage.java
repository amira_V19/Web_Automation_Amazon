package pages;

import helper.GUIActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class CartPage  {
    private WebDriver driver;
    private final GUIActions guiActions;

    public CartPage(WebDriver driver) {
        this.driver=driver;
        guiActions=new GUIActions(driver);
    }

    By addProductButton=By.id("add-to-cart-button");
    By cartProducts=By.cssSelector(".a-section.a-spacing-mini.sc-list-body.sc-java-remote-feature");
    By saveForLaterBttn=By.cssSelector("input.a-color-link[value=\"Save for later\"]");
    By saveLaterProducts=By.partialLinkText("KODAK Mini 2 Plus Retro Mobile 10 X 15 Photo Print");
    By moveToBasketBttn=By.cssSelector("input.a-button-input[value=\"Move to Basket\"]");
    By numberOfItems=By.id("sc-subtotal-label-buybox");
    By statusOfShoppingCard=By.tagName("h2");



    public void setAddProductButton() {
        guiActions.clickButton(addProductButton);
    }
    public String checkCartProducts() {
        return guiActions.getText(cartProducts);
    }
    public void saveForLater() {
        guiActions.clickButton(saveForLaterBttn);
    }
    public String checkSaveLaterProducts() {
        return guiActions.getText(saveLaterProducts);
    }
    public void setMoveToBasketBttn() {
        guiActions.clickButton(moveToBasketBttn);
    }
    public String setNumberOfItems() {
        return guiActions.getText(numberOfItems);
    }
    public String statusOfShoppinCart() {
        return guiActions.getText(statusOfShoppingCard);
    }
}
