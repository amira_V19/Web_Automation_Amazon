package pages;

import helper.GUIActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import static helper.GUIActions.clickButton;

public class SelectProductPage {
    private final WebDriver driver;
    private final GUIActions guiActions;

    public SelectProductPage(WebDriver driver)
    {
        this.driver = driver;
        guiActions=new GUIActions(driver);
    }

    By selectFirstOption = By.partialLinkText("Welczek WK-01 Handheld Printer Portable with 5 Inch Touch Screen, Hand Inkjet Printe");
    By selectSecondOption = By.partialLinkText("KODAK Mini 2 Plus Retro Mobile 10 X 15 Photo Printer, and Polaroid Instant Pictures");

    public void setSelectFirstOption() {
        clickButton(selectFirstOption);
    }
    public void setSelectSecondOption() {
        clickButton(selectSecondOption);
    }
}
