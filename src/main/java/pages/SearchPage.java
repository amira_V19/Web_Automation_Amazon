package pages;

import helper.GUIActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static helper.GUIActions.*;

public class SearchPage {
    private final WebDriver driver;
    private final GUIActions guiActions;

    public SearchPage(WebDriver driver)
    {
        this.driver = driver;
        guiActions=new GUIActions(driver);
    }

    By searchBox = By.id("twotabsearchtextbox");
    By searchBttn = By.id("nav-search-submit-button");


    public void searchForProduct(String item)
    {
        setText(searchBox, item);
        clickButton(searchBttn);
    }

}
