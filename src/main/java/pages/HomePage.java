package pages;

import helper.GUIActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import static helper.GUIActions.clickButton;

public class HomePage
{
    WebDriver driver;
    Actions actions;
    private final GUIActions guiActions;

    public HomePage(WebDriver driver)
    {
        this.driver = driver;
        guiActions = new GUIActions(driver);
        actions = new Actions(driver);
    }


    private final By acceptCookies=By.id("sp-cc-accept");
    private final By accountLists=By.id("nav-link-accountList");
    private final By startHere=By.xpath("//*[@id=\"nav-flyout-ya-newCust\"]/a");
    private final By signIN= By.id("nav-flyout-ya-signin");
    private final By shoppingCart=By.id( "nav-cart-count");


    public RegisterationPage clickStartHere()
    {
        WebElement action= driver.findElement(accountLists);
        actions.moveToElement(action).perform();
        clickButton(startHere);
        return new RegisterationPage(driver);
    }

    public LoginPage clickSignIn()
    {
        WebElement action= driver.findElement(accountLists);
        actions.moveToElement(action).perform();
        clickButton(signIN);
        return new LoginPage(driver);
    }

    public void openShoppingCart() {
        clickButton(shoppingCart);
    }
}
