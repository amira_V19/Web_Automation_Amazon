package pages;

import helper.GUIActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import static helper.GUIActions.*;

public class RegisterationPage {
    private final WebDriver driver;
    private final GUIActions guiActions;

    public RegisterationPage(WebDriver driver)
    {
        this.driver = driver;
        guiActions=new GUIActions(driver);
    }

    By nameTxt = By.id("ap_customer_name");
    By mobOrEmailTxt = By.id("ap_email");
    By passwordTxt = By.id("ap_password");
    By passwordCheckTxt = By.id("ap_password_check");
    By submitButton = By.id("continue");


    public void userRegister(String fullName, String mobOrEmail, String password, String passCheck)
    {
        setText(nameTxt, fullName);
        setText(mobOrEmailTxt, mobOrEmail);
        setText(passwordTxt, password);
        setText(passwordCheckTxt, passCheck);
        clickButton(submitButton);
    }

}
