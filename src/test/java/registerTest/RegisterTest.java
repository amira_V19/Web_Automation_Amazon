package registerTest;

import base.BaseTests;
import helper.ConfigProperties;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.RegisterationPage;

import java.io.IOException;

public class RegisterTest extends BaseTests {
    RegisterationPage registerationPage;
    HomePage homePage;
    ConfigProperties ReadData = new ConfigProperties();
    @Test
    public void userCanRegistSuccssfully() throws IOException {

        ReadData.ReadConfigDataProperties();
        String name=ReadData.getName();
        String email=ReadData.getEmail();
        String password=ReadData.getPassword();
        homePage = new HomePage(driver);
        registerationPage = new RegisterationPage(driver);
        homePage.clickStartHere();
        registerationPage.userRegister(name, email, password, password);

    }


}
