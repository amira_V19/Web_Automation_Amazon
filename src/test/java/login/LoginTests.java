package login;

import base.BaseTests;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import helper.ConfigProperties;
import java.io.IOException;

public class LoginTests extends BaseTests {
    HomePage homePage;
    LoginPage loginPage;

    ConfigProperties ReadData = new ConfigProperties();

    @Test
    public void testSuccessfulLogin() throws IOException {

        ReadData.ReadConfigDataProperties();
        String email=ReadData.getEmail();
        String password=ReadData.getPassword();
        System.out.println(password);
        homePage = new HomePage(driver);
        loginPage = new LoginPage(driver);
        homePage.clickSignIn();
        loginPage.setMobOrEmail(email);
        loginPage.setPassword(password);
        Assert.assertTrue(loginPage.getAlertText().contains("Amira"), "Error login");
    }
}




