package base;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

public class BaseTests {
    public static WebDriver driver;

    @BeforeClass
    @Parameters( ("browser") )
    public void StartDriver(@Optional( "chrome" ) String browserName) {
        if (browserName.equalsIgnoreCase("chrome")) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
        }

        driver.manage().window().maximize();
        driver.navigate().to("https://www.amazon.de/-/en/ref=nav_logo");

    }

    @AfterClass
    public void closeDriver()
    {
        driver.quit();  }

}



