package addProductTest;

import base.BaseTests;
import login.LoginTests;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.CartPage;
import pages.HomePage;
import pages.SearchPage;
import pages.SelectProductPage;

import java.io.IOException;
import java.time.Duration;

public class AddProductTest extends BaseTests {
    SelectProductPage selectProductPage;
    SearchPage searchPage;
    HomePage homePage;
    CartPage cartPage;
    LoginTests loginTests;

    @Test()
    public void loginAndAddProductSuccessfully() throws IOException {
        loginTests =new LoginTests();
        searchPage = new SearchPage(driver);
        selectProductPage = new SelectProductPage(driver);
        cartPage = new CartPage(driver);

        loginTests.testSuccessfulLogin();
        searchPage.searchForProduct("Drucker");
        selectProductPage.setSelectFirstOption();
        cartPage.setAddProductButton();
        searchPage.searchForProduct("Drucker");
        selectProductPage.setSelectSecondOption();
        cartPage.setAddProductButton();
    }

    @Test()
    public void validateShoppingBasketSuccessfully() {
        searchPage = new SearchPage(driver);
        homePage = new HomePage(driver);
        selectProductPage = new SelectProductPage(driver);
        cartPage = new CartPage(driver);
        homePage.openShoppingCart();
        Assert.assertFalse(cartPage.statusOfShoppinCart().contains("Your Amazon basket is empty"), "Basket is empty!");
        Assert.assertTrue(cartPage.checkCartProducts().contains("Welczek WK-01 Handheld Printer Portable with 5 Inch Touch Screen, Hand Inkjet Printer")
                , "you didn't add the first product");
        Assert.assertTrue(cartPage.checkCartProducts().contains("KODAK Mini 2 Plus Retro Mobile 10 X 15 Photo Printer, and Polaroid Instant Pictures print")
                , "you didn't add the second product");
        cartPage.saveForLater();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        Assert.assertTrue(cartPage.checkSaveLaterProducts().contains("KODAK Mini 2 Plus Retro Mobile 10 X 15 Photo Pri")
                , "SaveLater Button Didn't Work");
        cartPage.setMoveToBasketBttn();
        homePage.openShoppingCart();
        Assert.assertTrue(cartPage.setNumberOfItems().contains("2"));
    }


}